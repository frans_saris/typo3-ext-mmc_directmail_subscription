#
# Add email_verification_code to 'tt_address'
#
CREATE TABLE tt_address (
	email_verification_code varchar(255) DEFAULT '' NOT NULL,
);
