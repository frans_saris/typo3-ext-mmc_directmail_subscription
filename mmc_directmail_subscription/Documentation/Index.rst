﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
MMC directmail subscription
=============================================================

.. only:: html

	:Classification:
		mmc_directmail_subscription

	:Version:
		|release|

	:Language:
		en

	:Description:
		Custom direct-mail subscription built with extbase

	:Keywords:
		direct_mail, subscription, extbase, honeypot, de, fr, it

	:Copyright:
		2015

	:Author:
		Matthias Mächler

	:Email:
		maechler@mm-computing.ch

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 5
	:titlesonly:
	:glob:

	Introduction/Index
	User/Index
	Administrator/Index
	Configuration/Index
	Developer/Index
	KnownProblems/Index
	ToDoList/Index
	ChangeLog/Index
	Targets
