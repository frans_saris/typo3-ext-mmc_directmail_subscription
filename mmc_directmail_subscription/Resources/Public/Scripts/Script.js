var mmcDmlSubscr = {
	
	submitForm: function(){
		if( mmcDmlSubscr.validateForm( '.tx-mmc-directmail-subscription .mmc-dms-form' ) ){
			$('.tx-mmc-directmail-subscription .mmc-dms-form #toph-2').val('hpot');
			return true;
		}
		return false;
	},
	
	validateForm: function( formSelector ){
		var formValid = true;
		$( formSelector + ' .form-row .validate').each(function(){
			$(this).closest('.form-row').removeClass('error');
			var fieldValid = true;
			if( $(this).hasClass('required') && ! $(this).val() ){
				fieldValid = false;
			}
			if ( $(this).hasClass('email') && ! $(this).val().match(/^|(\S+@\S+\.\S+)$/) ){
				fieldValid = false;			
			}
			if( !fieldValid ){
				$(this).closest('.form-row').addClass('error');
				if( formValid )
					$(this).focus();
				formValid = false;
			}
		});
		return formValid;
	}
	
}